# docker-python-gis

Docker image containing common GIS packages.

To build the image.
```
docker build -t ccrpc/python-gis:latest .
```

To run a script
```
docker run -it -v "$(pwd)/code":/code/ image-name python -u /code/script.py
```

To run a script with the python interperter in the docker container.
```
./run.sh /code/hello.py
```

To run a script with a local PostGIS docker container
```
docker run -it -v "$(pwd)/code":/code/ --network=gis_default ccrpc/python-gis python -u /code/networkmerge.py
```
