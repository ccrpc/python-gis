FROM python:3.7-slim

# extra metadata
LABEL version="0.1"
LABEL description="Image containing common GIS python libraries"

ENV PYTHONUNBUFFERED 1
WORKDIR /build
ADD requirements.txt /build/

VOLUME /code

RUN apt update \
  && apt install -y \
  gcc \
  python3-rtree \
  python3-dev \
  libgdal-dev \
  build-essential \
  gdal-bin \
  && pip install --no-cache-dir -r requirements.txt \
  && apt remove -y \
  gcc \
  python3-dev \
  libgdal-dev \
  build-essential \
  && rm -r /var/lib/apt/lists/* \
  && apt -qy autoremove
